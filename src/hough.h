/**
 * @file hough.h
 * @brief header file for Hough Transformation
 * @author Ryou Ohsawa
 * @year 2020
 */
#ifndef __HOUGH_H_INCLUDE
#define __HOUGH_H_INCLUDE

#include <cstdlib>
#include <memory>
#include <vector>
#include <array>
#include <cmath>


namespace hough {
  typedef std::vector<double> axis;
  typedef std::unique_ptr<double[]> pixmap;


  pixmap
  make_pixmap(const size_t& N)
  {
    return pixmap(new double[N]);
  }

  pixmap
  generate_hough_transformed_map
  ( const double* input,
    const size_t& Nx, const size_t& Ny,
    const double& r0, const double& r1, const size_t& Nr,
    const double& t0, const double& t1, const size_t& Nt,
    const double& width=1.0)
  {
    axis naxis1, naxis2;
    axis range, theta;
    pixmap output(new double[Nr*Nt]), count(new double[Nr*Nt]);
    const double dr = (r1-r0)/(Nr-1.0);
    const double dt = (t1-t0)/(Nt-1.0);
    for (size_t i=0; i<Nx; i++) naxis1.push_back(-(Nx-1.)/2.+i);
    for (size_t i=0; i<Ny; i++) naxis2.push_back(-(Ny-1.)/2.+i);
    for (size_t i=0; i<Nr; i++) range.push_back(r0+dr*i);
    for (size_t i=0; i<Nt; i++) theta.push_back(t0+dt*i);
    for (size_t i=0; i<Nr*Nt; i++) output[i] = 0.0;

    const double dw = width*dr;
#pragma omp parallel shared(naxis1,naxis2,theta)
#pragma omp for schedule(dynamic) nowait
    for (size_t j=0; j<Ny; j++) {
      for (size_t i=0; i<Nx; i++) {
        for (size_t k=0; k<Nt; k++) {
          const double& t = theta[k];
          const double D = -cos(t)*naxis1[i]+sin(t)*naxis2[j];
          const long l0 = floor((-r0+D-dw)/dr+0.5);
          const long l1 = floor((-r0+D+dw)/dr+0.5);
          for (long l=l0; l<l1; l++) {
            if (l>=0 && l<(long)Nr) {
              output[Nt*l+k] += input[Nx*j+i];
              count[Nt*l+k]  += 1.0;
            }
          }
        }
      }
    }

    for (size_t i=0; i<Nr*Nt; i++) {
      if (count[i]>0) output[i] /= sqrt(count[i]);
    }
    return output;
  }

  pixmap
  generate_hough_transformed_map
  ( const std::vector<double>& input,
    const size_t& Nx, const size_t& Ny,
    const double& r0, const double& r1, const size_t& Nr,
    const double& t0, const double& t1, const size_t& Nt,
    const double& width=1.0)
  {
    return generate_hough_transformed_map
      ((double*)input.data(),Nx,Ny,r0,r1,Nr,t0,t1,Nt,width);
  }

  pixmap
  generate_hough_transformed_map
  ( const pixmap& input,
    const size_t& Nx, const size_t& Ny,
    const double& r0, const double& r1, const size_t& Nr,
    const double& t0, const double& t1, const size_t& Nt,
    const double& width=1.0)
  {
    return generate_hough_transformed_map
      ((double*)input.get(),Nx,Ny,r0,r1,Nr,t0,t1,Nt,width);
  }

}
#endif // __HOUGH_H_INCLUDE
