/**
 * @file sample.cc
 * @brief test script
 * @author Ryou Ohsawa
 * @year 2020
 */
#include "hough.h"
#include <ctime>
#include <random>
#include <vector>


int
main(int argn, char** argv)
{
  clock_t t = clock();
  std::mt19937 gen; gen.seed(t);
  std::uniform_real_distribution<double> uniq(-1,1), brightness(0.1,10.0);
  std::normal_distribution<double> noise(0.0,1.0);

  const size_t naxis1(600), naxis2(400);
  const size_t nrx(401), ntx(91);
  const double r0(-350), r1(350), t0(-M_PI/2.), t1(M_PI/2.);

  const double F = brightness(gen), a=uniq(gen), b=uniq(gen);
  clock_t c0 = clock();
  hough::pixmap data = hough::make_pixmap(naxis1*naxis2);
  for (size_t n=0; n<naxis2; n++) {
    for (size_t m=0; m<naxis1; m++) {
      const double x = ((double)m-(naxis1-1)/2.0);
      const double y = ((double)n-(naxis2-1)/2.0);
      const double line = b*y-a*x+30;
      data[naxis1*n+m] = (noise(gen) + F*(std::abs(line)<1.));
    }
  }
  const hough::pixmap ptr =
    hough::generate_hough_transformed_map
    (data,naxis1,naxis2,r0,r1,nrx,t0,t1,ntx);
  for (size_t n=0; n<nrx; n++) {
    for (size_t m=0; m<ntx; m++) {
      double r = (r0+(r1-r0)*((double)n/(nrx-1)));
      double t = (t0+(t1-t0)*((double)m/(ntx-1)))*180.0/M_PI;
      printf("%lf %lf %.3lf %ld %ld\n", t,r,ptr[ntx*n+m],nrx*n+m,nrx*ntx);
    }
  }
  clock_t c1 = clock();
  printf("# elapsed time: %lf\n", (double)(c1-c0)/CLOCKS_PER_SEC/4);
  return 0;
}
